Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

  passwordless_for :users

  resources :users

  get "static/members_only", as: :members_only

  root to: "static#index"
end
